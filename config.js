export const SENDER_ID = `${Math.floor(Math.random() * 1000)}`;
export const SENDER_TYPE = 'mobileClient';
export const SERVER_ID = 'SERVER01';

export const WEBSOCKET_PORT = 8000;
export const SERVER_IP = "192.168.0.101"; // server IP address
