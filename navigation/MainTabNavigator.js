import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import { TabBarIcon } from '../components';
import {
  HomeScreen,
  SettingsScreen,
  AlertsScreen,
  CustomDeviceScreen,
  LightIntensityChartScreen
}  from '../screens';

import {
  HydrationDevicesScreen,
  SwitchScreen,
  TemperatureDevicesScreen,
  LightIntensityScreen
} from "../screens/devices";


const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
  initialRouteName: "Home"
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    Switch: SwitchScreen,
    Temperature: TemperatureDevicesScreen,
    Hydration: HydrationDevicesScreen,
    "Light Intensity": LightIntensityScreen,
    CustomDevice: CustomDeviceScreen,
    LightIntensityChart: LightIntensityChartScreen
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={Platform.OS === 'ios' ? `md-home${focused ? '' : ''}`
          : 'md-home'
      }
    />
  )
};


const SettingsStack = createStackNavigator(
  {
    Settings: SettingsScreen,
  },
  config
);

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={'md-options'} />
  )
};

SettingsStack.path = '';

const AlertsStack = createStackNavigator(
  {
    Alerts: AlertsScreen,
  },
  config
);

AlertsStack.navigationOptions = {
  tabBarLabel: 'Alerts',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={'md-archive'} />
  ),
};

SettingsStack.path = '';

const tabNavigator = createBottomTabNavigator({
  HomeStack,
  AlertsStack,
  SettingsStack
});

tabNavigator.path = '';

export default tabNavigator;
