
const Types = [
	{
		title: 'Switch',
		icon: 'md-switch',
		type: 'switch'
	},
	{
		title: 'Light Intensity',
		icon: 'md-sunny',
		type: 'lightSensor'
	},
	{
		title: 'Hydration',
		icon: 'md-flower',
		type: 'flowerHydration'
	},
	{
		title: 'Temperature',
		icon: 'md-thermometer',
		type: 'thermometer'
	},
	{
		title: 'Door locks',
		icon: 'md-lock',
		type: 'lock'
	}
];

const DeviceTypes = {
	SWITCH: 'switch',
	LIGHT_SENSOR: 'lightSensor',
};


export { DeviceTypes, Types };
