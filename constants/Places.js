import {
	faMitten,
	faBed,
	faUtensils,
	faCouch,
	faBath,
	faToilet,
	faGamepad,
	faBoxOpen,
	faCar,
	faShoePrints,
	faHome
} from '@fortawesome/free-solid-svg-icons';

import { library } from '@fortawesome/fontawesome-svg-core';
library.add(faMitten, faBed, faUtensils, faCouch, faBath, faToilet, faGamepad, faBoxOpen, faCar, faShoePrints, faHome);

const Places = {
	'blank': {
		icon: 'home',
		label: 'Other'
	},
	'kitchen': {
		icon: 'mitten',
		label: 'Kitchen',
	},
	'bedroom': {
		icon: 'bed',
		label: 'Bedroom',
	},
	'dining room': {
		icon: 'utensils',
		label: 'Dining Room',
	},
	'living room': {
		icon: 'couch',
		label: 'Living Room',
	},
	'bath room': {
		icon: 'bath',
		label: 'Bath Room',
	},
	'toilet': {
		icon: 'toilet',
		label: 'Toilet',
	},
	'play room': {
		icon: 'gamepad',
		label: 'Play Room',
	},
	'hall': {
		icon: 'shoe-prints',
		label: 'Hall',
	},
	'basement': {
		icon: 'car',
		label: 'Basement',
	},
	'attic': {
		icon: 'box-open',
		label: 'Attic'
	}
};

export { Places };
