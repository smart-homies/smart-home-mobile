
const Operations = {
	HELLO: 'hello',
	INFO: 'info',
	SENSOR_DATA: 'sensorData',
	REGISTER: 'register',
	REGISTERED: 'registered',
	NOT_REGISTERED: 'notRegistered',
	COMMAND: 'command',
	RESPONSE: 'response'
};

export { Operations };
