import React from 'react';
import { Text } from 'react-native';

function AntonioBoldText(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'antonio-bold'}]} />
  );
}

function AntonioLightText(props) {
  return (
    <Text {...props} style={[props.style, { fontFamily: 'antonio-light'}]} />
  );
}

export { AntonioBoldText, AntonioLightText };
