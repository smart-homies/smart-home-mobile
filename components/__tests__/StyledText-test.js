import React from 'react';
import renderer from 'react-test-renderer';

import { AntonioBoldText } from '../StyledText';

it(`renders correctly`, () => {
  const tree = renderer.create(<AntonioBoldText>Snapshot test!</AntonioBoldText>).toJSON();

  expect(tree).toMatchSnapshot();
});
