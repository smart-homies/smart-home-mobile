import React, { Component } from 'react';

import { Icon } from "react-native-elements";
import { AntonioLightText } from "./StyledText";
import {
	StyleSheet,
	View,
	TouchableOpacity,
} from "react-native";


class DevicesType extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { icon, title, onPress } = this.props;

		return (
			<View style={styles.container}>
				<TouchableOpacity onPress={onPress}>
				<Icon
					reverse
					raised
					name={icon}
					type={'ionicon'}
					size={40}
					color={'#8E2300'}
				/>
				</TouchableOpacity>
				<AntonioLightText style={styles.text}>{title}</AntonioLightText>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		marginRight: 10,
		marginTop: 20,
	},
	text: {
		textAlign: "center",
		fontSize: 19,
		marginTop: 5,
	}
});

export { DevicesType };
