import React, {Component} from "react";

import {
	Dimensions,
	StyleSheet,
	ScrollView
} from "react-native";
import { LineChart } from "react-native-chart-kit";
import { AntonioLightText } from "./StyledText";


class Chart extends Component {

	render() {
		const { deviceId, timestamps, data } = this.props;

		return(
			<ScrollView>
				<AntonioLightText style={styles.text}>{deviceId.toUpperCase()}</AntonioLightText>
				<LineChart
					data={{
						labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
						datasets: [
							{
								data: data
							}
						]
					}}
					width={Dimensions.get("window").width - 20}
					height={300}
					yAxisSuffix={"%"}
					chartConfig={{
						backgroundColor: "#8E2300",
						backgroundGradientFrom: "#8E2300",
						backgroundGradientTo: "#7555da",
						decimalPlaces: 0,
						color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
						labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
						style: {
							borderRadius: 16
						},
						propsForDots: {
							r: "6",
							strokeWidth: "2",
							stroke: "#8E2300"
						}
					}}
					style={styles.chart}
				/>
				{ timestamps.map((timestamp, i) => (
					<AntonioLightText key={i} style={styles.infoText}>{`${i+1}. ${timestamp.toUpperCase()} - ${data[i].toUpperCase()}%`}</AntonioLightText>
				))
				}
			</ScrollView>
		)
	}
}

const chartConfig = {
	backgroundGradientFrom: "#1E2923",
	backgroundGradientFromOpacity: 0,
	backgroundGradientTo: "#08130D",
	backgroundGradientToOpacity: 0.5,
	color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
	strokeWidth: 2,
	barPercentage: 0.5
};

const styles = StyleSheet.create({
	chart: {
		marginVertical: 8,
		borderRadius: 16,
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: {
		textAlign: "center",
		paddingVertical: 10,
		fontSize: 19,
		marginTop: 5,
	},
	infoText: {
		paddingHorizontal: 20,
		fontSize: 19,
		marginTop: 5,
	}
});

export { Chart };
