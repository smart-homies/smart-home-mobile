import React, { Component } from 'react';
import {
	Operations,
	DeviceTypes
} from "../constants";


function saveData(db, message, func) {
  if(message.operation === Operations.SENSOR_DATA) {
    db.devices.findOne({ deviceId: message.senderId }, function (err, docs) {
      if (docs != null) {
        console.log('UPDATE ->>', message.senderId);
        console.log(message);
        db.devices.update({ deviceId: message.senderId }, { $set: { value: message.value }},);
        func();
      }
      })
  } else if(message.operation === Operations.REGISTERED) {
    console.log('CLIENT REGISTERED');
    db.info.update({}, { $set: { registered: true }},);
  } else if(message.operation === Operations.RESPONSE) {
		if ("devices" in message) {
			message.devices.forEach(device =>
	      db.devices.findOne({ deviceId: device.deviceId }, function (err, docs) {
	        if (docs == null) {
	          console.log('INSERT ->>', device.deviceId);
	          device.deviceName = device.deviceId;
	          console.log(device);
	          db.devices.insert(device, function (err, doc) {});
	        }
	        func();
	      })
	    );
		} else {
			timestamps = [];
			data = [];
			message.data.forEach(element => {
				timestamps.push(element.timestamp), data.push(element.sensorData)
			});

			console.log('ADD SENSOR DATA ->>', message.data[0].deviceId);
			db.devices.update({ deviceId: message.data[0].deviceId }, { $set: { data: data, timestamps: timestamps }});
			func();
		}

  } else {
		console.log(message);
	}
	cleanMessageStorage(db);
	message.timestamp = new Date();
	db.messages.insert(message, function (err, doc) {});
}

function cleanMessageStorage(db) {
	if (db.messages.getAllData().length > 20) {
		db.messages.remove({}, { multi: true }, function (err, numRemoved) {});
	}
}

export { saveData };
