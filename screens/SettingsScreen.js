import React, {Component} from 'react';
import {StyleSheet, View} from "react-native";

import {ListItem} from "react-native-elements";
import { db, URL } from "./HomeScreen";

import {AntonioLightText} from "../components";
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";


class SettingsScreen extends Component {
  static navigationOptions = () => { return { headerTitle: 'Settings' }};

  constructor(props) {
    super(props);
    this.getTitle();
  }

  getTitle() {
    return db.info.getAllData()[0]['registered']
  }

  render() {
    return (
      <View>
        <ListItem
          title={<AntonioLightText style={styles.title}>{this.getTitle() ? 'Connected to server': 'Disconnected from server'}</AntonioLightText>}
          subtitle={<AntonioLightText style={styles.subtitle}>{URL}</AntonioLightText>}
          bottomDivider
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    marginLeft: 10
  },
  subtitle: {
    fontSize: 14,
    marginLeft: 10
  }
});

export { SettingsScreen };
