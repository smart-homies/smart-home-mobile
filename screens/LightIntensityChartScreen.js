import React, { Component } from 'react';
import {
	StyleSheet,
	View
} from 'react-native';

import {
	Chart,
	saveData
} from "../components";

import {
	SENDER_ID,
	SENDER_TYPE,
	SERVER_ID
} from "../config";

import {
	Operations
} from "../constants";


export const limit = 10;

class LightIntensityChartScreen extends Component {
  static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: 'Light Intensity Chart'
		};
	};

  constructor(props) {
		super(props);
		this.state = {
			deviceId: props.navigation.getParam('deviceId'),
			db: props.navigation.getParam('db'),
      ws: props.navigation.getParam('ws'),
      data: null,
			timestamps: null
		};

    this.state.ws.onmessage = (e) => {
			let data = JSON.parse(e.data);
			saveData(this.state.db, data, this.loadDataFromDB);
		};

		this.state.ws.onclose = () => {
			alert('The connection to the server has been lost.');
			this.state.db.info.update({ id: 0 }, { $set: { registered: false }},);
		};

		if(this.state.db.info.getAllData()[0]['registered']) {
			this.getSensorData();
		}

		this.loadDataFromDB = this.loadDataFromDB.bind(this);
	}

	componentDidMount() {
		this.loadDataFromDB();
	}

  getSensorData() {
    let getSensorData = {
      senderId: SENDER_ID,
      senderType: SENDER_TYPE,
			receiverId: SERVER_ID,
      operation: Operations.COMMAND,
			"command": "getSensorData",
      "deviceId": this.state.deviceId,
      "limit": limit
		};
		console.log('GET SENSOR DATA');
		console.log(getSensorData);
		this.state.ws.send(JSON.stringify(getSensorData));
  }

  loadDataFromDB() {
    this.state.db.devices.findOne({ deviceId: this.state.deviceId }, (err, docs) => {
			this.setState({
				data: docs.data,
				timestamps: docs.timestamps
			})
		});
  }

	render() {
	  return(
	    <View style={styles.container}>{
	      this.state.data && (
	        <Chart deviceId={this.state.deviceId} data={this.state.data} timestamps={this.state.timestamps}/>
	      )
	    }
	    </View>
	  )
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	iconContainer: {
		alignItems: 'center',
	},
	iconText: {
		marginTop: 10,
		fontSize: 16,
	},
	chart: {
		marginVertical: 8,
		borderRadius: 16,
		justifyContent: 'center',
		alignItems: 'center',
	}
});

export { LightIntensityChartScreen };
