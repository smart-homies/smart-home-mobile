import React, { Component } from 'react';
import { ListItem } from "react-native-elements";
import { ScrollView, StyleSheet } from "react-native";
import { db } from "./HomeScreen";
import { AntonioLightText } from "../components";


class AlertsScreen extends Component {
	static navigationOptions = () => { return { headerTitle: 'History' }};

	constructor(props) {
		super(props);

		this.state = {
			messages: []
		};
		this.loadDevicesFromDB = this.loadDevicesFromDB.bind(this);
	}

	componentDidMount() {
		this.loadDevicesFromDB();
	}

	loadDevicesFromDB() {
		this.setState({
			messages: db.messages.getAllData().sort((a, b) => a.timestamp < b.timestamp)
		});
	}

	render() {
		return(
			<ScrollView>
				{this.state.messages.map((device, i) => (
						<ListItem
							key={i}
							title={<AntonioLightText style={styles.title}>{device.senderId ? device.senderId.toUpperCase() : ""}</AntonioLightText>}
							subtitle={<AntonioLightText style={styles.subtitle}>{device.timestamp ? `${device.timestamp}` : ""}</AntonioLightText>}
							bottomDivider
							rightTitle={<AntonioLightText style={styles.rightTitle}>{device.operation ? `${device.operation.toUpperCase()}` : ""}</AntonioLightText>}
							rightSubtitle={<AntonioLightText style={styles.rightSubtitle}>{device.value ? `Data: ${device.value}` : ""}</AntonioLightText>}
						/>
					))
				}
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	header: {
		fontFamily: 'antonio-bold'
	},
	title: {
		fontSize: 16,
		marginLeft: 10
	},
	subtitle: {
		fontSize: 14,
		marginLeft: 10,
	},
	rightTitle: {
		fontSize: 14,
		marginRight: 10
	},
	rightSubtitle: {
		fontSize: 14,
		marginRight: 10
	},
});

export { AlertsScreen };
