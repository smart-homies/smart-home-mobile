import React, { Component } from 'react';
import {
	ScrollView,
	StyleSheet,
	Switch
} from 'react-native';

import {
	ListItem
} from 'react-native-elements';

import {
	SENDER_ID,
	SENDER_TYPE,
	SERVER_ID
} from "../../config";

import {
	Operations,
	Places,
} from "../../constants";

import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import {
	AntonioLightText,
	saveData
 } from "../../components";


class SwitchScreen extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: navigation.getParam('title', 'No title'),
			};
	};

	constructor(props) {
		super(props);
		this.state = {
			devices: [],
			type: props.navigation.getParam('type'),
			ws: props.navigation.getParam('ws'),
			db: props.navigation.getParam('db')
		};

		this.state.ws.onmessage = (e) => {
			let data = JSON.parse(e.data);
			saveData(this.state.db, data, this.loadDevicesFromDB);
		};

		this.state.ws.onclose = () => {
			alert('The connection to the server has been lost.');
			this.state.db.info.update({ id: 0 }, { $set: { registered: false }},);
			this.loadDevicesFromDB();
		};

		if(this.state.db.info.getAllData()[0]['registered']) {
			// this.state.db.devices.remove({senderType: this.state.type },
			// 	 { multi: true }, function (err, numRemoved) {});
			this.getAllSensors();
		}
		this.loadDevicesFromDB = this.loadDevicesFromDB.bind(this);
	}

	getAllSensors() {
		let getAllSensors = { senderId: SENDER_ID, senderType: SENDER_TYPE,
			receiverId: SERVER_ID, operation: Operations.COMMAND,
			"command": "getSensor", "deviceType": this.state.type
		};
		console.log('GET ALL SENSORS');
		console.log(getAllSensors);
		this.state.ws.send(JSON.stringify(getAllSensors));
	}

	componentDidMount() {
		this.loadDevicesFromDB();
	}

	toggleSwitch = (value, deviceId) => {
		let changeSwitchValue = { senderId: SENDER_ID, senderType: SENDER_TYPE,
			receiverId: deviceId, operation: Operations.COMMAND,
			"command": "setValue", "value": value ? 1 : 0  };

		console.log('SEND MESSAGE');
		console.log(changeSwitchValue);
		this.state.ws.send(JSON.stringify(changeSwitchValue));

		this.state.db.devices.update({ deviceId: deviceId }, { $set: { value: value }});
		this.loadDevicesFromDB();
	};

	loadDevicesFromDB() {
		this.state.db.devices.find({ senderType: this.state.type }, (err, docs) => {
			this.setState({
				devices: docs
			})
		});
	}


	render() {

	return (
		<ScrollView>
			{this.state.devices.map((device, i) => (
				<ListItem
					key={i}
					title={<AntonioLightText style={styles.title}>{device.deviceName.toUpperCase()}</AntonioLightText>}
					subtitle={<AntonioLightText style={styles.subtitle}>{Places[device.icon ?? 'blank'].label}</AntonioLightText>}
					rightIcon={
						this.state.db.info.getAllData()[0]['registered'] &&
						(<Switch
							onValueChange = {() => this.toggleSwitch(!device.value, device.deviceId)}
							value = {!!device.value}
						/> )}
					onLongPress={() => this.props.navigation.navigate('CustomDevice', {
						device: device, db: this.state.db, screen: "Switch", refresh: this.loadDevicesFromDB })}
					bottomDivider
					leftIcon={<FontAwesomeIcon icon={Places[device.icon ?? 'blank'].icon} color={'#8E2300'} size={30} chevron/>}
					chevron
				/>
			))
			}
		</ScrollView>
		)
	}
	}

const styles = StyleSheet.create({
	header: {
		fontFamily: 'antonio-bold'
	},
	title: {
		fontSize: 19,
		marginLeft: 10
	},
	subtitle: {
		fontSize: 14,
		marginLeft: 10,
	}
});

export { SwitchScreen };
