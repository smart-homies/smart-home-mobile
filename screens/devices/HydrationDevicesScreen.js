import React, { Component } from 'react';


class HydrationDevicesScreen extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: navigation.getParam('title', 'No title'),
		};
	};
}

export { HydrationDevicesScreen };