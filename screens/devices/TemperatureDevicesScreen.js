import React, { Component } from 'react';


class TemperatureDevicesScreen extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: navigation.getParam('title', 'No title'),
		};
	};
}

export { TemperatureDevicesScreen };