import React, { Component } from 'react';
import {
	View,
	ScrollView,
	StyleSheet
} from 'react-native';

import {
	ListItem
} from 'react-native-elements';

import {
	AntonioLightText,
	saveData
} from "../../components";

import {
	SENDER_ID,
	SENDER_TYPE,
	SERVER_ID
} from "../../config";

import {
	Operations,
	Places,
} from "../../constants";

import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import ProgressCircle from 'react-native-progress-circle'


class LightIntensityScreen extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: navigation.getParam('title', 'Chart'),
		};
	};

	constructor(props) {
		super(props);
		this.state = {
			devices: [],
			type: props.navigation.getParam('type'),
			ws: props.navigation.getParam('ws'),
			db: props.navigation.getParam('db')
		};

		this.state.ws.onmessage = (e) => {
			let data = JSON.parse(e.data);
			saveData(this.state.db, data, this.loadDevicesFromDB);
		};

		this.state.ws.onclose = () => {
			alert('The connection to the server has been lost.');
			this.state.db.info.update({ id: 0 }, { $set: { registered: false }},);
			this.loadDevicesFromDB();
		};

		if(this.state.db.info.getAllData()[0]['registered']) {
			// 		this.state.db.devices.remove({}, { multi: true }, function (err, numRemoved) {});
			this.getAllSensors();
		}
		this.loadDevicesFromDB = this.loadDevicesFromDB.bind(this);
	}

	loadDevicesFromDB() {
		this.state.db.devices.find({ senderType: this.state.type }, (err, docs) => {
			this.setState({
				devices: docs
			})
		});
	}

	getAllSensors() {
		let getAllSensors = { senderId: SENDER_ID, senderType: SENDER_TYPE,
			receiverId: SERVER_ID, operation: Operations.COMMAND,
			"command": "getSensor", "deviceType": this.state.type
		};
		console.log('GET ALL SENSORS');
		console.log(getAllSensors);
		this.state.ws.send(JSON.stringify(getAllSensors));
	}

	componentDidMount() {
		this.loadDevicesFromDB();
	}

	render() {

	return (
		<ScrollView>
			{this.state.devices.map((device, i) => (
				<ListItem
					key={i}
					title={<AntonioLightText style={styles.title}>{device.deviceName.toUpperCase()}</AntonioLightText>}
					subtitle={<AntonioLightText style={styles.subtitle}>{Places[device.icon ?? 'blank'].label}</AntonioLightText>}
					rightIcon={
						this.state.db.info.getAllData()[0]['registered'] &&
						(
							<ProgressCircle
            percent={device.value}
            radius={30}
            borderWidth={8}
            color={'#8E2300'}
            shadowColor={'#D8D8D8'}
            bgColor="#fff"
        >
            <AntonioLightText style={{ fontSize: 14 }}>{`${device.value}%`}</AntonioLightText>
        </ProgressCircle>
					)}
					onLongPress={() => this.props.navigation.navigate("CustomDevice", {
						device: device,
						db: this.state.db,
						screen: "LightIntensity",
						refresh: this.loadDevicesFromDB
					})}
					onPress={() => this.props.navigation.navigate("LightIntensityChart", {
						deviceId: device.deviceId,
						db: this.state.db,
						ws: this.state.ws,
						screen: "LightIntensity",
						refresh: this.loadDevicesFromDB
					})}
					bottomDivider
					leftIcon={<FontAwesomeIcon icon={Places[device.icon ?? 'blank'].icon} color={'#8E2300'} size={30} chevron/>}
					chevron
				/>
			))
			}
		</ScrollView>
		)}
	}


const styles = StyleSheet.create({
	header: {
		fontFamily: 'antonio-bold'
	},
	title: {
		fontSize: 19,
		marginLeft: 10
	},
	subtitle: {
		fontSize: 14,
		marginLeft: 10,
	}
});


export { LightIntensityScreen };
