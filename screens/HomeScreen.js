import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Platform
} from 'react-native';

import { Operations, Types } from '../constants';
import { DevicesType, AntonioBoldText } from '../components';
import { SENDER_ID, SENDER_TYPE, SERVER_ID, WEBSOCKET_PORT, SERVER_IP } from "../config";
import Datastore from "react-native-local-mongodb";


export const URL = `ws://${SERVER_IP}:${WEBSOCKET_PORT}/`;
export const ws = new WebSocket(URL);

export const db = {
  messages: new Datastore({filename: 'messages', autoload: true}),
  info: new Datastore({filename: 'info', autoload: true}),
  devices: new Datastore({filename: 'devices', autoload: true})
};

ws.onopen = () => {
  console.log('CONNECTED');
  let register = { senderId: SENDER_ID, senderType: SENDER_TYPE,
    receiverId: SERVER_ID, operation: Operations.REGISTER, payload: "" };

  console.log('REGISTER CLIENT');
  console.log(register);
  ws.send(JSON.stringify(register));
};

ws.onclose = () => {
  alert('The connection to the server has been lost.');
  db.info.update({ id: 0 }, { $set: { registered: false }},);
};

ws.onmessage = (e) => {
  let message = JSON.parse(e.data);
  if(message.operation === Operations.REGISTERED) {
    console.log('CLIENT REGISTERED');
    db.info.update({ id: 0 }, { $set: { registered: true }},);
  }
  message.timestamp = new Date();
  db.messages.insert(message, function (err, doc) {});
};

class HomeScreen extends Component {
  constructor(props) {
		super(props);
    db.info.findOne({ id: 0 }, function (err, docs) {
      if (docs == null) {
        db.info.insert({ id: 0, registered: false }, function (err, doc) {});
      } else {
        db.info.update({ id: 0 }, { $set: { registered: false }},);
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <AntonioBoldText style={styles.iconText}>{"SMART HOME"}</AntonioBoldText>
        </View>
        <ScrollView contentContainerStyle={styles.devicesContainer}>
          {
            Types.map((type, i) => (
              <DevicesType
                key={i}
                icon={type.icon}
                title={type.title.toUpperCase()}
                onPress={() => this.props.navigation.navigate(type.title,
                  { title: type.title, type: type.type, ws: ws, db: db })}
              />
            ))}
        </ScrollView>
      </View>
    );
  }
}

HomeScreen.navigationOptions = {
  header: null,
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  iconContainer: {
    alignItems: 'center',
    marginTop: 40,
  },
  iconImage: {
    width: 20,
    height: 10,
    marginTop: 10,
    resizeMode: 'contain'
  },
  iconText: {
    marginTop: 10,
    fontSize: 45,
  },
  devicesContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    marginLeft: Platform.OS === 'ios' ? 30 : 50
  }
});

export { HomeScreen };
