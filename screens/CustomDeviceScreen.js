import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	TextInput,
	Platform
} from 'react-native';

import {
	Icon,
	Button
} from "react-native-elements";

import { AntonioLightText } from "../components";
import { Places } from "../constants";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";

function items(dict, fn) {
	return Object.keys(dict).map((key, i) => {
		if (key !== 'blank'){
			return fn(key, dict[key], i)
		}
	})
}

class CustomDeviceScreen extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: 'Custom device'
		};
	};

	constructor(props) {
		super(props);
		this.state = {
			device: props.navigation.getParam('device'),
			db: props.navigation.getParam('db'),
			deviceName: null,
			deviceIcon: null
		};
	}

	saveChanges() {
		if (this.state.deviceName != null) {
			this.state.db.devices.update({ deviceId: this.state.device.deviceId }, { $set: {
					deviceName: this.state.deviceName }});
		}
		if (this.state.deviceIcon != null) {
			this.state.db.devices.update({ deviceId: this.state.device.deviceId }, { $set: {
					icon: this.state.deviceIcon }});
		}
	}

	goBack() {
    const { navigation } = this.props;
		navigation.state.params.refresh()
    navigation.goBack();
  }

	render() {
		return (
			<View style={styles.container}>
				<AntonioLightText style={styles.text}>{'Change device name'}</AntonioLightText>
				<TextInput
					style={
						Platform.OS === 'ios'
							? pickerSelectStyles.inputIOS
							: pickerSelectStyles.inputAndroid
					}
					placeholder="Type new device name"
					onChangeText={(text) => this.setState({deviceName: text})}
					value={this.state.text}
				/>
					<AntonioLightText style={styles.text}>{'Choose icon'}</AntonioLightText>
				<View style={styles.devicesContainer}>
					{
						items(Places, (key, value) => (
							<Button
								type="clear"
								key={key}
								icon={<FontAwesomeIcon
									icon={value.icon}
									color={'#8E2300'}
									size={45}
									chevron/>}
								style={styles.iconButton}
								onPress={() => this.setState({deviceIcon: key})}
							/>
						))
					}
				</View>
				<Button
					title="Save changes"
					titleStyle={styles.buttonTitle}
					buttonStyle={styles.button}
					onPress={() => {this.saveChanges(); this.goBack()}}
				/>
			</View>
		)
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	button: {
		backgroundColor: '#8E2300',
		marginTop: 60,
		marginHorizontal: 80
	},
	text: {
		paddingHorizontal: 20,
		paddingVertical: 10,
		fontSize: 19,
		marginTop: 5,
	},
	buttonTitle: {
		fontFamily: 'antonio-light',
	},
	devicesContainer: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'flex-start',
		paddingHorizontal: 10,
	},
	iconButton: {
		paddingHorizontal: 5,
		paddingVertical: 5
	}
});

const pickerSelectStyles = StyleSheet.create({
	inputIOS: {
		fontSize: 16,
		fontFamily: 'antonio-light',
		paddingVertical: 12,
		paddingHorizontal: 10,
		borderWidth: 1,
		borderColor: '#8E2300',
		borderRadius: 4,
		color: 'black',
		paddingRight: 30,
		marginRight: 20,
		marginLeft: 20
	},
	inputAndroid: {
		fontSize: 16,
		fontFamily: 'antonio-light',
		paddingHorizontal: 10,
		paddingVertical: 8,
		borderWidth: 0.5,
		borderColor: '#8E2300',
		borderRadius: 8,
		color: 'black',
		paddingRight: 30, // to ensure the text is never behind the icon
	},
});

export { CustomDeviceScreen };
